package com.mobile.beetm.addstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.helper.ItemOnClickListener;
import com.mobile.beetm.addstore.model.MainItem;
import com.mobile.beetm.addstore.util.CategoryUtil;
import com.mobile.beetm.addstore.util.LocationUtil;
import com.mobile.beetm.addstore.util.StringConvertUtil;
import com.mobile.beetm.addstore.view.ItemDetailsActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PopularItemAdapter extends RecyclerView.Adapter<PopularItemAdapter.ViewHolder> {

    private Context mContext;
    private List<MainItem> mMainItemList;
    private ItemOnClickListener onClickListener;


    public PopularItemAdapter(Context mContext, List<MainItem> mMainItemList) {
        this.mContext = mContext;
        this.mMainItemList = mMainItemList;
    }

    public void setOnClickListener(ItemOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_main_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MainItem mainItem = mMainItemList.get(position);
        String title = StringConvertUtil.removeSpecialChar(mainItem.title);
        holder.txtTitle.setText(StringConvertUtil.setFirstLetterCapitel(title));
        String categoryName = CategoryUtil.getCategoryStringById(mainItem.category);
        String locationName = LocationUtil.getLocationStringById(mainItem.city);
        holder.txtLocation.setText(locationName);
        holder.txtCategory.setText(categoryName);
        holder.txtEmail.setText(mainItem.email);
//        holder.txtPhoneNo.setText(mainItem.phoneNo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txtPhoneNo.setText(Html.fromHtml(mainItem.phoneNo, Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.txtPhoneNo.setText(Html.fromHtml(mainItem.phoneNo));
        }
        RequestOptions requestOptionsBannerImage = new RequestOptions();
        requestOptionsBannerImage.placeholder(R.drawable.default_banner_mobile);
        requestOptionsBannerImage.error(R.drawable.default_banner_mobile);

        if (Constant.ENABLE_IMAGE_CACHE) {
            requestOptionsBannerImage.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptionsBannerImage.skipMemoryCache(true);
        }
        Glide
                .with(mContext)
                .load(Constant.API_BASE_FILE_URL + mainItem.featuredImg)
                .placeholder(R.drawable.default_banner_mobile)
                .centerCrop()
                .into(holder.imgMain);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ItemDetailsActivity.class);
                intent.putExtra(Constant.KEY_ITEM, mainItem);
                intent.putExtra(Constant.KEY_CITY, locationName);
                intent.putExtra(Constant.KEY_TITLE, title);
                mContext.startActivity(intent);
            }
        });
//        if (mainItem.isOpening == null) {
//            mainItem.isOpening = StringConvertUtil.getOpeningHourList(mainItem.openingHour);
//        }
//        if (mainItem.isOpening) {
//            holder.txtOpenNow.setText("Open Now");
//        } else {
//            holder.txtOpenNow.setText("Closed");
//        }
    }



    @Override
    public int getItemCount() {
        return mMainItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imgMain)
        ImageView imgMain;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtEmail)
        TextView txtEmail;
        @BindView(R.id.txtCategory)
        TextView txtCategory;
        @BindView(R.id.txtPhoneNo)
        TextView txtPhoneNo;
//        @BindView(R.id.txtOpenNow)
//        TextView txtOpenNow;
        @BindView(R.id.txtLocation)
        TextView txtLocation;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onClickListener.ItemOnClickListener("popularItem", getAdapterPosition());
        }
    }
}
