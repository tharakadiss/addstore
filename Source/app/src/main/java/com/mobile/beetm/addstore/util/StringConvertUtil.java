package com.mobile.beetm.addstore.util;

import com.mobile.beetm.addstore.model.MainItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by Lasith Madhushanka on 3/16/2020
 */
public class StringConvertUtil {

    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

    public static String removeSpecialChar(String data) {
        String removeSomeSpecialChar = data.replaceAll("[<>\\[\\],-]", "");
        JSONObject object = null;
        String correctTitle = "";
        try {
            object = new JSONObject(removeSomeSpecialChar);
            correctTitle = object.getString("en");
        } catch (JSONException e) {
            correctTitle = removeSomeSpecialChar;
            e.printStackTrace();
        }
        return correctTitle;
    }

    public static String getStringWithoutSpecialChar(String data) {
        JSONObject object = null;
        String correctTitle = "";
        try {
            object = new JSONObject(data);
            correctTitle = object.getString("en");
        } catch (JSONException e) {
            correctTitle = data;
            e.printStackTrace();
        }
        return correctTitle;
    }

    public static String removeGalleryStringSpecialChar(String data) {
        String removeSomeSpecialChar = data.replaceAll("[<>\\[\\]-]\"", "");

        return removeSomeSpecialChar;
    }

    //get store open or not
    public static boolean getOpeningHourList(String openingHours) {
        JSONObject object = null;
        boolean isOpen = false;
        try {
            JSONArray jarray = new JSONArray(openingHours);
            int dayOfWeek = getTodaysDayOfWeek();
            MainItem.OpeningHours openingHours1 = new MainItem.OpeningHours();

            object = jarray.getJSONObject(dayOfWeek);
            openingHours1.close_time = object.getString("close_time");
            openingHours1.start_time = object.getString("start_time");
            openingHours1.day = object.getString("day");
            openingHours1.closed = object.getString("closed");

            Date startTime = sdf.parse(openingHours1.start_time);
            Date closeTime = sdf.parse(openingHours1.close_time);


            if (Calendar.getInstance().after(startTime) && Calendar.getInstance().before(closeTime)) {
                isOpen = true;
            } else {
                isOpen = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isOpen;
    }

    public static int getTodaysDayOfWeek() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_WEEK);
    }

    public static String setFirstLetterCapitel(String sentence){
        Scanner in = new Scanner(sentence.toLowerCase());
//        System.out.print("Input a Sentence: ");
        String line = in.nextLine();
        String upper_case_line = "";
        Scanner lineScan = new Scanner(line);
        while(lineScan.hasNext()) {
            String word = lineScan.next();
            upper_case_line += Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ";
        }
//        System.out.println(upper_case_line.trim());
     return upper_case_line.trim();
    }
}
