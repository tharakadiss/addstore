package com.mobile.beetm.addstore.util;

import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.model.Location;

/**
 * Created by Lasith Madhushanka on 3/16/2020
 */
public class LocationUtil {
    public static String getLocationStringById(String data) {
        String locationName = "";
        for (Location location : DataController.cityList) {
            if (location.id.equals(data)) {
                locationName = location.name;
                break;
            }
        }
        return locationName;
    }
}
