package com.mobile.beetm.addstore.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.helper.Constant;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lasith Madhushanka on 3/23/2020
 */
public class ItemSliderAdapter extends SliderViewAdapter<ItemSliderAdapter.SliderAdapterVH> {
    private Context context;
    private List<String> mSliderItems = new ArrayList<>();

    public ItemSliderAdapter(Context context) {
        this.context = context;
    }

    public void renewItems(List<String> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(String sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        String sliderItem = mSliderItems.get(position);

        String url= "";
        if (position == 0){
            url = Constant.API_BASE_FILE_URL;
        }else{
            url = Constant.API_BASE_FILE_GALLERY_URL;
        }
        viewHolder.textViewDescription.setText("");
        viewHolder.textViewDescription.setTextSize(16);
        viewHolder.textViewDescription.setTextColor(Color.WHITE);
        RequestOptions requestOptionsBannerImage = new RequestOptions();
        requestOptionsBannerImage.placeholder(R.drawable.default_banner_mobile);
        requestOptionsBannerImage.error(R.drawable.default_banner_mobile);
        if (Constant.ENABLE_IMAGE_CACHE) {
            requestOptionsBannerImage.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptionsBannerImage.skipMemoryCache(true);
        }
        Glide.with(viewHolder.itemView)
                .load(url + sliderItem)
                .placeholder(R.drawable.default_banner_mobile)
                .centerCrop()
                .into(viewHolder.imageViewBackground);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mSliderItems.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
