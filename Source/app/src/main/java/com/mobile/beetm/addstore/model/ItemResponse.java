package com.mobile.beetm.addstore.model;

import java.util.List;

/**
 * Created by Lasith Madhushanka on 3/15/2020
 */
public class ItemResponse {
    public List<MainItem> result;
    public String Message;
}
