package com.mobile.beetm.addstore.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.ybq.android.spinkit.SpinKitView;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.adapter.CategoryAdapter;
import com.mobile.beetm.addstore.adapter.PopularItemAdapter;
import com.mobile.beetm.addstore.api.Service;
import com.mobile.beetm.addstore.api.ServiceGenerator;
import com.mobile.beetm.addstore.helper.ConnectionChecker;
import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.helper.ItemOnClickListener;
import com.mobile.beetm.addstore.model.ItemResponse;
import com.mobile.beetm.addstore.model.MainItem;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ItemOnClickListener {

    @BindView(R.id.recyclerCategory)
    RecyclerView recyclerCategory;
    @BindView(R.id.recyclerMain)
    RecyclerView recyclerMain;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;

    private Call getItemCall;

    private ConnectionChecker connectionChecker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        connectionChecker = new ConnectionChecker(this);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerCategory.setLayoutManager(layoutManager);
        CategoryAdapter categoryAdapter = new CategoryAdapter(DataController.categoryList, this);
        recyclerCategory.setAdapter(categoryAdapter);


        if (connectionChecker.isInternetAvailable()) {
            callItemList();
        } else {
            dialogNoInternet();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void callItemList() {
        Service service = ServiceGenerator.createService(Service.class);
        getItemCall = service.getItemList("TopList", 0, 20);
        getItemCall.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {
                spinKit.setVisibility(View.GONE);
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().result != null) {
                            loadRecyclerView(response.body().result);
                        } else {
                            dialogOccurError();
                        }
                    } else {
                        dialogOccurError();
                    }
                } else {
                    dialogOccurError();
                }
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                spinKit.setVisibility(View.GONE);
                if (connectionChecker.isInternetAvailable()) {
                    dialogOccurError();
                } else {
                    dialogNoInternet();
                }
            }
        });
    }

    private void loadRecyclerView(List<MainItem> result) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerMain.setLayoutManager(linearLayoutManager);
        PopularItemAdapter itemAdapter = new PopularItemAdapter(this, result);
        recyclerMain.setAdapter(itemAdapter);
        itemAdapter.setOnClickListener(this);
    }


    @OnClick(R.id.txtSearch)
    public void txtSearchTouch() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);

    }



    @Override
    public void onBackPressed() {
        openAlertDialog();

    }



    @Override
    public void ItemOnClickListener(String type, int position) {
        Intent intent = new Intent(this, ItemDetailsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        if (getItemCall != null) {
            getItemCall.cancel();
        }
        super.onDestroy();
    }

    public void dialogNoInternet() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_no_internet, null);
        LayoutNoInternetDialog internetDialog = new LayoutNoInternetDialog(dialogView);

        internetDialog.btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    class LayoutNoInternetDialog {
        @BindView(R.id.btnDialog)
        Button btnDialog;

        public LayoutNoInternetDialog(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void dialogOccurError() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_show_message, null);
        LayoutLoadingErrorDialog dialog = new LayoutLoadingErrorDialog(dialogView);

        dialog.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();
                alertDialog.dismiss();
            }
        });
        dialog.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    class LayoutLoadingErrorDialog {
        @BindView(R.id.btnSubmit)
        Button btnSubmit;
        @BindView(R.id.btnCancel)
        Button btnCancel;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        public LayoutLoadingErrorDialog(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void openAlertDialog() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_show_message, null);
        LayoutLoadingErrorDialog dialog = new LayoutLoadingErrorDialog(dialogView);
        dialog.txtTitle.setText(R.string.are_you_sure_you_want_to_close_app);
        dialog.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.super.onBackPressed();
                alertDialog.dismiss();
            }
        });
        dialog.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

}
