package com.mobile.beetm.addstore.model;

/**
 * Created by Lasith Madhushanka on 3/16/2020
 */
public class Location {
    public String id;
    public String name;
    public String parent;
}
