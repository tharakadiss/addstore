package com.mobile.beetm.addstore.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.github.ybq.android.spinkit.SpinKitView;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.adapter.ItemAdapter;
import com.mobile.beetm.addstore.api.Service;
import com.mobile.beetm.addstore.api.ServiceGenerator;
import com.mobile.beetm.addstore.helper.ConnectionChecker;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.model.Category;
import com.mobile.beetm.addstore.model.ItemResponse;
import com.mobile.beetm.addstore.model.MainItem;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemActivity extends AppCompatActivity {

    @BindView(R.id.recyclerItem)
    RecyclerView recyclerItem;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.spinCategory)
    Spinner spinCategory;
    @BindView(R.id.spinSubCategory)
    Spinner spinSubCategory;
    @BindView(R.id.layout_item_not_found)
    LinearLayout layoutShowError;

    private int mBookMark = 0;
    private int rowCount = 15;
    private boolean isLoading = false;
    //    private int visibleThreshold = 5;
    private ItemAdapter itemAdapter;
    //    private GridLayoutManager layoutManager;
    LinearLayoutManager layoutManager;
    private List<MainItem> result;
    private boolean isScrolling = false;
    private int selectCategoryPosition = 0;
    private String category = "", subCategory = "", city = "", state = "", title = "", activityName = "";
    private boolean mIsCatSpinerFirstCall = true;
    private boolean mIsSubCatSpinerFirstCall = true;
    private Context mContext;
    private Call call;

    public List<String> categoryStringList = new ArrayList<>();
    public List<String> subCategoryStringList = new ArrayList<>();
    public List<Category> setTempCategoryList;
    private double longitude, latitude;
    private ConnectionChecker connectionChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_green_36dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mContext = this;
        connectionChecker = new ConnectionChecker(this);
        result = new ArrayList<>();

        if (getIntent() != null) {
            activityName = getIntent().getStringExtra(Constant.ACTIVITY_NAME);
            if (activityName != null && activityName.equals("SEARCH_ACTIVITY")) {
                city = getIntent().getStringExtra(Constant.KEY_CITY);
                state = getIntent().getStringExtra(Constant.KEY_STATE);
                title = getIntent().getStringExtra(Constant.KEY_TITLE);
                longitude = getIntent().getDoubleExtra(Constant.KEY_LONGITUDE, 0);
                latitude = getIntent().getDoubleExtra(Constant.KEY_LONGITUDE, 0);
                title = getIntent().getStringExtra(Constant.KEY_TITLE);
            }
            setTempCategoryList = new ArrayList<>();
            mIsCatSpinerFirstCall = true;
            mIsSubCatSpinerFirstCall = true;
            category = getIntent().getStringExtra(Constant.KEY_CATEGORY_ID);


        }
        if (connectionChecker.isInternetAvailable()) {
            callItemList(mBookMark);
        } else {
            dialogNoInternet();
        }

        setCategorySpinner(category);
        setSubCategorySpinner();
    }

    @OnClick(R.id.btnPageRefresh)
    public void clickedPageRefreshBtn() {
        recreate();
    }


    private void setSubCategorySpinner() {
        subCategoryStringList.clear();
        subCategoryStringList.add("All");
        setTempCategoryList.clear();
        for (Category category : DataController.subCategoryList) {
            if (category.parent.equals(this.category)) {
                setTempCategoryList.add(category);
                subCategoryStringList.add(category.title);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, subCategoryStringList);
        spinSubCategory.setAdapter(adapter);
        spinSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!mIsSubCatSpinerFirstCall) {
                    mBookMark = 0;
                    subCategory = setTempCategoryList.get((position - 1)).id;
                    callItemList(mBookMark);

                } else {
                    mIsSubCatSpinerFirstCall = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setCategorySpinner(String categoryId) {
        categoryStringList.clear();
        categoryStringList.add("All");
        int i = 1;
        for (Category category : DataController.categoryList) {
            String titles[] = category.title.split("Store");
            categoryStringList.add(titles[0]);
            if (category.id.equals(categoryId)) {
                selectCategoryPosition = i;
            }
            i++;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, categoryStringList);
        spinCategory.setAdapter(adapter);
        spinCategory.setSelection(selectCategoryPosition);

        spinCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!mIsCatSpinerFirstCall) {
                    mIsSubCatSpinerFirstCall = true;
                    category = DataController.categoryList.get((position - 1)).id;
                    setSubCategorySpinner();
                    mBookMark = 0;
                    callItemList(mBookMark);
                } else {
                    mIsCatSpinerFirstCall = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void callItemList(int count) {
        spinKit.setVisibility(View.VISIBLE);
        Service service = ServiceGenerator.createService(Service.class);

        call = service.getSearchedItemList("GetSearchResultPosts", state, latitude, longitude, city, category, subCategory
                , title, count, rowCount);

        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {
                spinKit.setVisibility(View.GONE);
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().result != null) {
                            if (mBookMark == 0) {
                                result.clear();
                            }
                            layoutShowError.setVisibility(View.GONE);
                            result.addAll(response.body().result);
                            loadRecyclerView(result);
                        } else {
                            if (mBookMark == 0) {
                                setErrorMsg(Constant.KEY_SEARCH_RESULT_NOT_FOUND);
                                result.clear();
                                loadRecyclerView(result);
                                layoutShowError.setVisibility(View.VISIBLE);
                            }

                        }
                    } else {
                        if (connectionChecker.isInternetAvailable()) {
                            setErrorMsg(Constant.KEY_SERVER_ISSUE);
                        } else {
                            setErrorMsg(Constant.KEY_INTERNET_CONNECTION_ISSUE);
                        }
                        result.clear();
                        loadRecyclerView(result);
                        layoutShowError.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (connectionChecker.isInternetAvailable()) {
                        setErrorMsg(Constant.KEY_SERVER_ISSUE);
                    } else {
                        setErrorMsg(Constant.KEY_INTERNET_CONNECTION_ISSUE);
                    }
                    result.clear();
                    loadRecyclerView(result);
                    layoutShowError.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                spinKit.setVisibility(View.GONE);
                if (connectionChecker.isInternetAvailable()) {
                    setErrorMsg(Constant.KEY_SERVER_ISSUE);
                } else {
                    setErrorMsg(Constant.KEY_INTERNET_CONNECTION_ISSUE);
                }
                result.clear();
                loadRecyclerView(result);
                layoutShowError.setVisibility(View.VISIBLE);
            }
        });
    }

    public void setErrorMsg(String type) {
        if (type.equals(Constant.KEY_SERVER_ISSUE)) {
            dialogOccurError();
        } else if (type.equals(Constant.KEY_INTERNET_CONNECTION_ISSUE)) {
            dialogNoInternet();
        }
    }

    private void loadRecyclerView(List<MainItem> itemList) {
        if (itemAdapter == null) {
            layoutManager = new LinearLayoutManager(this);
            recyclerItem.setLayoutManager(layoutManager);
            itemAdapter = new ItemAdapter(this, itemList);

            recyclerItem.setAdapter(itemAdapter);
        } else if (mBookMark == 0) {
            itemAdapter.notifyDataSetChanged();
        } else {
            isLoading = false;
            itemAdapter.notifyItemRangeInserted(mBookMark, rowCount);
        }


        recyclerItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                Log.d("SCROLL_STATE", recyclerView.getScrollState() + "");
                int totalItemCount = layoutManager.getItemCount();

                if (isScrolling && !isLoading && ((visibleItemCount + pastVisibleItems)) >= totalItemCount) {
                    isLoading = true;
                    isScrolling = false;
                    mBookMark = mBookMark + 15;
                    callItemList(mBookMark);
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }

        return super.onOptionsItemSelected(item);
    }


    public void dialogNoInternet() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_no_internet, null);
        LayoutNoInternetDialog internetDialog = new LayoutNoInternetDialog(dialogView);

        internetDialog.btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    class LayoutNoInternetDialog {
        @BindView(R.id.btnDialog)
        Button btnDialog;

        public LayoutNoInternetDialog(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void dialogOccurError() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_show_message, null);
        LayoutLoadingErrorDialog dialog = new LayoutLoadingErrorDialog(dialogView);

        dialog.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();
                alertDialog.dismiss();
            }
        });
        dialog.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    class LayoutLoadingErrorDialog {
        @BindView(R.id.btnSubmit)
        Button btnSubmit;
        @BindView(R.id.btnCancel)
        Button btnCancel;

        public LayoutLoadingErrorDialog(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
