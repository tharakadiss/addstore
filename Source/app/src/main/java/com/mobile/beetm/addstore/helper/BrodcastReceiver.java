package com.mobile.beetm.addstore.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by Lasith Madhushanka on 3/9/2020
 */
public class BrodcastReceiver extends BroadcastReceiver {

    NetConnectionListner connectionListner;

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            connectionListner.netConnectionStatus(true);
//            Toast.makeText(context,"Connected",Toast.LENGTH_LONG).show();
        }else{
            connectionListner.netConnectionStatus(false);
//            Toast.makeText(context,"Faield",Toast.LENGTH_LONG).show();

        }
    }

    public  void setConnectionListner(NetConnectionListner connectionListner) {
        this.connectionListner = connectionListner;
    }
}
