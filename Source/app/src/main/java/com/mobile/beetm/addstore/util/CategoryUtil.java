package com.mobile.beetm.addstore.util;

import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.model.Category;

/**
 * Created by Lasith Madhushanka on 3/16/2020
 */
public class CategoryUtil {
    public static String getCategoryStringById(String data) {
        String categoryName = "";
        for (Category category : DataController.categoryList) {
            if (category.id.equals(data)) {
                categoryName = category.title;
                break;
            }
        }
        if (categoryName.isEmpty()) {
            for (Category category : DataController.subCategoryList) {
                if (category.id.equals(data)) {
                    categoryName = category.title;
                    break;
                }
            }
        }
        return categoryName;
    }
}
