package com.mobile.beetm.addstore.helper;

import com.mobile.beetm.addstore.model.Category;
import com.mobile.beetm.addstore.model.Location;

import java.util.ArrayList;
import java.util.List;

public class DataController {
    public static List<Category> categoryList = new ArrayList<>();
    public static List<Category> subCategoryList = new ArrayList<>();
    public static List<Location> districtList = new ArrayList<>();
    public static List<Location> cityList = new ArrayList<>();
}
