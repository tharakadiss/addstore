package com.mobile.beetm.addstore.helper;

/**
 * Created by Lasith Madhushanka on 3/9/2020
 */
public interface NetConnectionListner {
    public void netConnectionStatus(boolean isConnected);
}
