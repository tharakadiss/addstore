package com.mobile.beetm.addstore.view;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.ybq.android.spinkit.SpinKitView;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.api.Service;
import com.mobile.beetm.addstore.api.ServiceGenerator;
import com.mobile.beetm.addstore.helper.BrodcastReceiver;
import com.mobile.beetm.addstore.helper.ConnectionChecker;
import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.helper.DataDownloder;
import com.mobile.beetm.addstore.helper.NetConnectionListner;
import com.mobile.beetm.addstore.model.CategoryResponse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements NetConnectionListner {

    private Context mContext;
//    private ConnectionChecker connectionChecker;

    @BindView(R.id.uiSplash)
    RelativeLayout splash;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.layout_item_not_found)
    LinearLayout layoutShowError;


    BrodcastReceiver receiver;
    boolean isRegisterReceiver = true;
    IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        mContext = this;
//        connectionChecker = new ConnectionChecker(this);

        receiver = new BrodcastReceiver();
        receiver.setConnectionListner(this);
        filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGE");

    }

//    @Override
//    protected void onResume() {
//        if (connectionChecker.isInternetAvailable()) {
//            getCategoryList(getString(R.string.main_category_all));
//        } else {
//            dialogNoInternet();
//        }
//        super.onResume();
//    }

    @Override
    protected void onResume() {
        registerReceiver(receiver, filter);
        isRegisterReceiver = true;
        super.onResume();
    }

    @OnClick(R.id.btnPageRefresh)
    public void clickedPageRefreshBtn() {
        recreate();
    }


    private void getCategoryList(String type) {
        new ServiceGenerator();
        Service service = ServiceGenerator.createService(Service.class);
        Call mCategoryCall = service.getCategoryList(type);

        mCategoryCall.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call
                    , Response<CategoryResponse> response) {

                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().result != null) {
                            if (type.equals(getString(R.string.main_category_all))) {
                                DataController.categoryList = response.body().result;
                                getCategoryList(getString(R.string.sub_category_all));
                            } else {
                                spinKit.setVisibility(View.GONE);
                                DataController.subCategoryList = response.body().result;
                                callNextActivity();
                            }
                        } else {
                            spinKit.setVisibility(View.GONE);
                        }
                    } else {
                        spinKit.setVisibility(View.GONE);
                    }
                } else {
                    spinKit.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                spinKit.setVisibility(View.GONE);
            }
        });
    }

    public void callNextActivity() {
        DataDownloder.getLocationList("DistrictList");
        DataDownloder.getLocationList("CityList");
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        finish();
    }


    public void dialogNoInternet() {
        final androidx.appcompat.app.AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_box_no_internet, null);
        LayoutNoInternetDialog internetDialog = new LayoutNoInternetDialog(dialogView);

        internetDialog.btnDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    class LayoutNoInternetDialog {
        @BindView(R.id.btnDialog)
        Button btnDialog;

        public LayoutNoInternetDialog(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void netConnectionStatus(boolean isConnected) {
        if (isConnected) {
            getCategoryList(getString(R.string.main_category_all));
        } else {
            dialogNoInternet();
        }


    }

    @Override
    protected void onPause() {
        if (isRegisterReceiver) {
            unregisterReceiver(receiver);
            isRegisterReceiver = false;
        }
        super.onPause();
    }


}
