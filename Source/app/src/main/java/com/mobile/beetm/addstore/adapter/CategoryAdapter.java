package com.mobile.beetm.addstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.helper.IconHelper;
import com.mobile.beetm.addstore.model.Category;
import com.mobile.beetm.addstore.view.ItemActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<Category> categories;
    Context context;
    private IconHelper mIconHelper;

    public CategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
        mIconHelper = new IconHelper(context, 20, R.color.colorAccent);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_category_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category category = categories.get(position);
        String title[] = category.title.split("Store");
        holder.mTxtCategory.setText(title[0]);
        String iconName = setWStringSet(category.faIcon);
        try {
            holder.imgCategory.setImageDrawable(mIconHelper.getDrawable(FontAwesome.Icon.valueOf(iconName)));
        } catch (Exception e) {
            if (iconName.equals("faw_shopping")){
                holder.imgCategory.setImageDrawable(mIconHelper.getDrawable(FontAwesome.Icon.faw_shopping_cart));
            }else if(iconName.equals("faw_cutlery")){
                holder.imgCategory.setImageDrawable(mIconHelper.getDrawable(FontAwesome.Icon.faw_utensils));
            }else{
                holder.imgCategory.setImageDrawable(mIconHelper.getDrawable(FontAwesome.Icon.faw_school));
            }
//            holder.imgCategory.setImageDrawable(mIconHelper.getDrawable(FontAwesome.Icon.faw_school));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ItemActivity.class);
                intent.putExtra(Constant.ACTIVITY_NAME,"MAIN_ACTIVITY");
                intent.putExtra(Constant.KEY_CATEGORY_ID, category.id);
                context.startActivity(intent);
            }
        });
    }


    private String setWStringSet(String iconName) {
        String s[] = iconName.split("-");
        String t = s[1];
        if (t.equals("television")) {
            t = "tv";
        }
        return "faw_" + t;
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtCategory)
        public TextView mTxtCategory;
        @BindView(R.id.imgCategory)
        ImageView imgCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
