package com.mobile.beetm.addstore.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.helper.DataController;
import com.mobile.beetm.addstore.model.Category;
import com.mobile.beetm.addstore.model.Location;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.spinCategory)
    Spinner spinCategory;
    @BindView(R.id.spinDistrict)
    Spinner spinDistrict;
    @BindView(R.id.spinCity)
    Spinner spinCity;
    @BindView(R.id.txtSearch)
    TextView txtSearch;


    private List<String> cityStringList;
    private List<String> districtStringList;
    private String stateId = "";
    private String categoryId = "";
    private String cityId = "";

    public List<String> categoryStringList = new ArrayList<>();
    public List<String> subCategoryStringList = new ArrayList<>();
    public List<Category> tempCategoryList;
    double longitude, latitude;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        spinCity.setEnabled(false);
        tempCategoryList = new ArrayList<>();
        tempCategoryList = DataController.categoryList;
        districtStringList = new ArrayList<>();


        setDistrictSpinner();
        setCategorySpinner();
        setCitySpinner("");

    }

    private void setCitySpinner(String s) {
        cityStringList = new ArrayList<>();
        cityStringList.add("Select City");
        if (!s.isEmpty()) {
            for (Location location : DataController.cityList) {
                if (location.parent.equals(s)) {
                    cityStringList.add(location.name);
                }
            }
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.layout_category_spinner, cityStringList) {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public View getView(int position, View convertView, ViewGroup parent) {
                if (position == 0) {
                    TextView tv = (TextView) super.getView(0, convertView, parent);
//                    tv.setTextColor(getResources().getColor(R.color.silver));
                    tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location
                            , 0, 0, 0);
                    return tv;
                } else {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
//                    tv.setTextColor(getResources().getColor(R.color.black));
                    return tv;
                }
            }
        };
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, cityStringList);
        spinCity.setAdapter(adapter);
        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view.findViewById(R.id.spinCategory);
                tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
                tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location
                        , 0, 0, 0);
                getCityId(cityStringList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setDistrictSpinner() {
        districtStringList.add("Select District");
        for (Location location : DataController.districtList) {
            districtStringList.add(location.name);
        }
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, districtStringList);
        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.layout_category_spinner, districtStringList) {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public View getView(int position, View convertView, ViewGroup parent) {
                if (position == 0) {
                    TextView tv = (TextView) super.getView(0, convertView, parent);
//                    tv.setTextColor(getResources().getColor(R.color.silver));
                    tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check
                            , 0, 0, 0);
                    return tv;
                } else {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
//                    tv.setTextColor(getResources().getColor(R.color.black));
                    return tv;
                }
            }
        };
        spinDistrict.setAdapter(adapter);
        spinDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    TextView tv = (TextView) view.findViewById(R.id.spinCategory);
//                    tv.setTextColor(getResources().getColor(R.color.silver));
                    tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check
                            , 0, 0, 0);
                    setCitySpinner(getStateId(districtStringList.get(position)));
                    spinCity.setEnabled(true);
                } else {
                    spinCity.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getStateId(String s) {
        boolean isEqual = false;
        for (Location location : DataController.districtList) {
            if (location.name.equals(s)) {
                stateId = location.id;
                isEqual = true;
            }
        }
        if (!isEqual) {
            stateId = "";
        }
        return stateId;
    }

    private void getCityId(String s) {
        boolean isEqual = false;
        for (Location location : DataController.cityList) {
            if (location.name.equals(s)) {
                cityId = location.id;
                isEqual = true;
            }
        }
        if (!isEqual) {
            cityId = "";
        }
    }

    private void getCategoryId(String s) {
        boolean isEqual = false;
        for (Category category : DataController.categoryList) {
            if (category.title.contains(s)) {
                categoryId = category.id;
                isEqual = true;
            }
        }

        if (!isEqual) {
            categoryId = "";
        }

    }


    private void setCategorySpinner() {
        categoryStringList.add("Select Category");
        for (Category category : DataController.categoryList) {
            String titles[] = category.title.split("Store");
            categoryStringList.add(titles[0]);
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.layout_category_spinner, categoryStringList) {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public View getView(int position, View convertView, ViewGroup parent) {
                if (position == 0) {
                    TextView tv = (TextView) super.getView(0, convertView, parent);
//                    tv.setTextColor(getResources().getColor(R.color.silver));
                    tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));

                    tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu
                            , 0, 0, 0);
                    return tv;
                } else {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
//                    tv.setTextColor(getResources().getColor(R.color.black));
                    return tv;
                }
            }
        };
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.layout_category_spinner, Constant.categoryStringList);
        spinCategory.setAdapter(adapter);
        spinCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view.findViewById(R.id.spinCategory);
                tv.setCompoundDrawableTintList(getResources().getColorStateList(R.color.silver));
                tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_menu
                        , 0, 0, 0);
                getCategoryId(categoryStringList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btnCancel)
    public void btnCancelPressed() {
        onBackPressed();
    }


    @OnClick(R.id.btnSubmit)
    public void btnSubmit() {
        Intent intent = new Intent(this, ItemActivity.class);

//        getLocation();

        intent.putExtra(Constant.ACTIVITY_NAME, "SEARCH_ACTIVITY");
        intent.putExtra(Constant.KEY_TITLE, txtSearch.getText().toString());

        intent.putExtra(Constant.KEY_STATE, stateId);
        intent.putExtra(Constant.KEY_CITY, cityId);
        intent.putExtra(Constant.KEY_CATEGORY_ID, categoryId);
        intent.putExtra(Constant.KEY_LONGITUDE, longitude);
        intent.putExtra(Constant.KEY_LATITUDE, latitude);
        startActivity(intent);
    }

    private void getLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        longitude = location.getLongitude();
        latitude = location.getLatitude();
    }

}
