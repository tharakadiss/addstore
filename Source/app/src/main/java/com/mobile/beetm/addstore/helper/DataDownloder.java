package com.mobile.beetm.addstore.helper;

import com.mobile.beetm.addstore.api.Service;
import com.mobile.beetm.addstore.api.ServiceGenerator;
import com.mobile.beetm.addstore.model.LocationResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataDownloder {

    public static void getLocationList(String type) {

        Service service = ServiceGenerator.createService(Service.class);
        Call mCategoryCall = service.getLocationList(type);

        mCategoryCall.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call
                    , Response<LocationResponse> response) {
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().result != null) {
                            if (type.equals("CityList")) {
                                DataController.cityList = response.body().result;
                            } else {
                                DataController.districtList = response.body().result;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
            }
        });
    }

}
