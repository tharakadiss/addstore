package com.mobile.beetm.addstore.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.model.MainItem;
import com.mobile.beetm.addstore.util.LocationUtil;
import com.mobile.beetm.addstore.util.StringConvertUtil;
import com.mobile.beetm.addstore.view.ItemDetailsActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private Context mContext;
    private List<MainItem> mMainItemList;

    public ItemAdapter(Context mContext, List<MainItem> mMainItemList) {
        this.mContext = mContext;
        this.mMainItemList = mMainItemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_category_select_item, parent, false);
        return new ItemAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MainItem mainItem = mMainItemList.get(position);
        String title = StringConvertUtil.removeSpecialChar(mainItem.title);
        holder.txtTitle.setText(title);
        String locationName = LocationUtil.getLocationStringById(mainItem.city);
        holder.txtLocation.setText(locationName);
//        holder.txtEmail.setText(mainItem.email);
//        if (mainItem.rating == 0.0) {
//
//            holder.ratingBar.setVisibility(View.GONE);
////            txtReviewCount.setVisibility(View.GONE);
//        } else {
        holder.ratingBar.setVisibility(View.VISIBLE);
        holder.ratingBar.setRating(mainItem.rating);

//        }
         String phone = secondPhoneNoForNewLine(mainItem.phoneNo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txtPhoneNo.setText(Html.fromHtml(phone, Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.txtPhoneNo.setText(Html.fromHtml(phone));
        }
        RequestOptions requestOptionsBannerImage = new RequestOptions();
        requestOptionsBannerImage.placeholder(R.drawable.default_banner_mobile);
        requestOptionsBannerImage.error(R.drawable.default_banner_mobile);
        if (Constant.ENABLE_IMAGE_CACHE) {
            requestOptionsBannerImage.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptionsBannerImage.skipMemoryCache(true);
        }
        Glide
                .with(mContext)
                .load(Constant.API_BASE_FILE_URL + mainItem.featuredImg)
                .placeholder(R.drawable.default_banner_mobile)
                .centerCrop()
                .into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ItemDetailsActivity.class);
                intent.putExtra(Constant.KEY_ITEM, mainItem);
                intent.putExtra(Constant.KEY_CITY, locationName);
                intent.putExtra(Constant.KEY_TITLE, title);
                mContext.startActivity(intent);
            }
        });
    }

    public String secondPhoneNoForNewLine(String phone) {
        String result = phone;
        String[] splitPhoneNo = phone.split("/");
        if (splitPhoneNo.length == 2) {
//            result  = splitPhoneNo[0].trim() + "\n" +splitPhoneNo[1].trim();
            result = splitPhoneNo[0].trim();
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return mMainItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
//        @BindView(R.id.txtEmail)
//        TextView txtEmail;
@BindView(R.id.ratingBar)
RatingBar ratingBar;
        @BindView(R.id.txtLocation)
        TextView txtLocation;
        @BindView(R.id.txtPhoneNo)
        TextView txtPhoneNo;
        @BindView(R.id.imgMain)
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
