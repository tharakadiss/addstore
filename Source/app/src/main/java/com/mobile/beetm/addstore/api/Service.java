package com.mobile.beetm.addstore.api;

import com.mobile.beetm.addstore.model.CategoryResponse;
import com.mobile.beetm.addstore.model.ItemResponse;
import com.mobile.beetm.addstore.model.LocationResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface Service {

    @FormUrlEncoded
    @POST("categories.php")
    Call<CategoryResponse> getCategoryList(@Field("req_method") String type);

    @FormUrlEncoded
    @POST("locations_list.php")
    Call<LocationResponse> getLocationList(@Field("req_method") String type);

    @FormUrlEncoded
    @POST("post_list.php")
    Call<ItemResponse> getItemList(@Field("req_method") String type,
                                   @Field("offset") int offset, @Field("rowcount") int rawCount);

    @FormUrlEncoded
    @POST("post_list.php")
    Call<ItemResponse> getRelatedItemList(@Field("req_method") String type, @Field("id") String id,
                                          @Field("catid") String catid,
                                          @Field("offset") int offset, @Field("rowcount") int rawCount);

    @FormUrlEncoded
    @POST("post_list.php")
    Call<ItemResponse> getSearchedItemList(@Field("req_method") String type,@Field("stateid") String stateid
                                           ,@Field("latitude") Double latitude ,@Field("longitude") Double longitude
                                            ,@Field("cityid") String cityid,
                                           @Field("catid") String catid,@Field("subcatid") String subCatid,
                                           @Field("title") String title,
                                           @Field("offset") int offset, @Field("rowcount") int rawCount);
}
