package com.mobile.beetm.addstore.view;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mobile.beetm.addstore.R;
import com.mobile.beetm.addstore.adapter.ItemSliderAdapter;
import com.mobile.beetm.addstore.adapter.PopularItemAdapter;
import com.mobile.beetm.addstore.api.Service;
import com.mobile.beetm.addstore.api.ServiceGenerator;
import com.mobile.beetm.addstore.helper.Constant;
import com.mobile.beetm.addstore.model.ItemResponse;
import com.mobile.beetm.addstore.model.MainItem;
import com.mobile.beetm.addstore.util.StringConvertUtil;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txtCity)
    TextView txtCity;
    //    @BindView(R.id.txtRating)
//    TextView txtRating;
//    @BindView(R.id.txtReviewCount)
//    TextView txtReviewCount;
    @BindView(R.id.txtPhone)
    TextView txtPhone;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    //    @BindView(R.id.txtWeb)
//    TextView txtWeb;
    @BindView(R.id.txtLocation)
    TextView txtLocation;
    //    @BindView(R.id.txtOpenNow)
//    TextView txtOpenNow;
//    @BindView(R.id.txtDescription)
//    TextView txtDescription;
    @BindView(R.id.imgGallery)
    ImageView imgGallery;
    @BindView(R.id.txtDescription)
    WebView webView;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.spin_kit)
    SpinKitView spinKit;
    @BindView(R.id.txtYouMayAlsoLike)
    TextView txtYouMayAlsoLike;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;

    SliderView sliderView;
    private ItemSliderAdapter adapter;

    private GoogleMap mMap;
    private MainItem mainItem;
    private String city, title;
    private boolean isOnlyTumbImage;
    private Context context;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        ButterKnife.bind(this);

        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setElevation(0);
        context = this;
        appBarLayout.addOnOffsetChangedListener((appBarLayout, offset) -> {
            final boolean isCollapsed = (offset == (-1 * appBarLayout.getTotalScrollRange()));

            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this,
                    isCollapsed ?
                            R.drawable.ic_chevron_left_black_24dp :
                            R.drawable.ic_chevron_left_green_36dp));
        });

        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent() != null) {
            mainItem = getIntent().getParcelableExtra(Constant.KEY_ITEM);


            city = getIntent().getStringExtra(Constant.KEY_CITY);
            title = getIntent().getStringExtra(Constant.KEY_TITLE);
        }


        toolbarLayout.setTitle(title);
        toolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this,R.color.normal_transparent));
        toolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));


        setImgGallery();

        JsonParser jsonParser = new JsonParser();
        // Convert JSON Array String into JSON Array
        List<String> galleryList = new ArrayList<>();
        System.out.println(mainItem.featuredImg);

        if (mainItem.gallery != null && !mainItem.gallery.equals("[]")) {
            isOnlyTumbImage = false;
            if (mainItem.featuredImg != null && !mainItem.featuredImg.equals("[]")) {
                galleryList.add(mainItem.featuredImg);
            }
            String jsonArrayString = mainItem.gallery;
            JsonArray arrayFromString = jsonParser.parse(jsonArrayString).getAsJsonArray();

            for (JsonElement s : arrayFromString) {
                String removeEx = s.toString().replaceAll("^\"|\"$", "");
                galleryList.add(removeEx);
            }

        } else if (mainItem.featuredImg != null && !mainItem.featuredImg.equals("[]")) {
            isOnlyTumbImage = true;
            galleryList.add(mainItem.featuredImg);
        } else {
            galleryList.clear();
            galleryList.add("defaultBanner");

        }
        // if receive one image hide slider view and show image view
        if (galleryList.size() == 1) {
            String url = "";
            if (isOnlyTumbImage) {
                url = Constant.API_BASE_FILE_URL;
            } else {
                url = Constant.API_BASE_FILE_GALLERY_URL;
            }
            imgGallery.setVisibility(View.VISIBLE);
            sliderView.setVisibility(View.GONE);
            Glide.with(ItemDetailsActivity.this)
                    .load(url + galleryList.get(0))
                    .placeholder(R.drawable.default_banner_mobile)
                    .centerCrop()
                    .into(imgGallery);
        } else {
            imgGallery.setVisibility(View.GONE);
            sliderView.setVisibility(View.VISIBLE);
            adapter.renewItems(galleryList);
        }


        setUIData();
        callItemList();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

//    @OnTouch(R.id.txtPhone)
//    public void txtTouchPhone(){
//        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
//            android.text.ClipboardManager clipboard = (android.text.ClipboardManager)
//                    context.getSystemService(Context.CLIPBOARD_SERVICE);
//            clipboard.setText(txtPhone.getText());
//        } else {
//            android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
//                    context.getSystemService(Context.CLIPBOARD_SERVICE);
//            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", txtPhone.getText());
//            clipboard.setPrimaryClip(clip);
//
//        }
//        Toast.makeText(getApplicationContext(), "Text Copied",
//                Toast.LENGTH_SHORT).show();
//    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setImgGallery() {
        sliderView = findViewById(R.id.imageSlider);

        adapter = new ItemSliderAdapter(this);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.SWAP);

//        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.getSliderPager().stopNestedScroll();
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
//        sliderView.setIndicatorSelectedColor(R.color.yellow);
//        sliderView.setIndicatorUnselectedColor(R.color.black);
        sliderView.setIndicatorVisibility(true);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(false);


    }


    private void setUIData() {
        String titles[] = title.split("Store");
        txtTitle.setText(titles[0]);
        txtCity.setText(city);
//        if (mainItem.rating.equals("0.0")) {
//
//            txtRating.setVisibility(View.GONE);
//            txtReviewCount.setVisibility(View.GONE);
//        } else {
//            txtRating.setText(mainItem.rating);
//
//        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtPhone.setText(Html.fromHtml(mainItem.phoneNo, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtPhone.setText(Html.fromHtml(mainItem.phoneNo));
        }
//        txtPhone.setText(mainItem.phoneNo);
        txtEmail.setText(mainItem.email);
//        if (mainItem.website.isEmpty()) {
//            txtWeb.setVisibility(View.GONE);
//            txtReviewCount.setVisibility(View.GONE);
//        } else {
//
//            txtWeb.setText("Web - " + mainItem.website);
//        }
        String location = StringConvertUtil.getStringWithoutSpecialChar(mainItem.address);
        txtLocation.setText(location);
        String description = StringConvertUtil.getStringWithoutSpecialChar(mainItem.description);

        webView.loadData(description, "text/html", "utf-8");
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            txtDescription.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            txtDescription.setText(Html.fromHtml(description));
//        }

//        if (mainItem.isOpening == null) {
//            mainItem.isOpening = StringConvertUtil.getOpeningHourList(mainItem.openingHour);
//        }
//        if (mainItem.isOpening) {
//            txtOpenNow.setText("Open Now");
//        } else {
//            txtOpenNow.setText("Closed");
//        }

    }

    private void callItemList() {
        Service service = ServiceGenerator.createService(Service.class);
        Call call = service.getRelatedItemList("RelatedPosts", mainItem.id, mainItem.category, 0, 20);
        call.enqueue(new Callback<ItemResponse>() {
            @Override
            public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {
                spinKit.setVisibility(View.GONE);
                if (response != null) {
                    if (response.body() != null) {
                        if (response.body().result != null) {
                            loadRecyclerView(response.body().result);
                        } else {
                            txtYouMayAlsoLike.setVisibility(View.GONE);
                        }
                    } else {
                        txtYouMayAlsoLike.setVisibility(View.GONE);
                    }
                } else {
                    txtYouMayAlsoLike.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ItemResponse> call, Throwable t) {
                txtYouMayAlsoLike.setVisibility(View.GONE);
                spinKit.setVisibility(View.GONE);
            }
        });
    }

    private void loadRecyclerView(List<MainItem> result) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(linearLayoutManager);
        PopularItemAdapter itemAdapter = new PopularItemAdapter(this, result);
        recycler.setAdapter(itemAdapter);
//        itemAdapter.setOnClickListener(this);
    }

//    @OnClick(R.id.btnBack)
//    public void backBtnPressed() {
//        onBackPressed();
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        float longitude = Float.parseFloat(mainItem.longitude);
        float latitude = Float.parseFloat(mainItem.latitude);
        // Add a marker in Sydney and move the camera
        LatLng location = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(location)).showInfoWindow();

        float zoomLevel = 11.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,zoomLevel));
        mMap.getUiSettings().setMapToolbarEnabled(true);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
        return false;
    }


//    @Override
//    public void ItemOnClickListener(String type, int position) {
//        Intent intent = new Intent(this, ItemDetailsActivity.class);
//        startActivity(intent);
//    }
}
