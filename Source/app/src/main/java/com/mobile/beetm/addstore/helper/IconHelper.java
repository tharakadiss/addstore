package com.mobile.beetm.addstore.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.typeface.IIcon;

public class IconHelper {

    Context mContext;
    int mSize;
    int mColor;
    Drawable mDrawable;

    public IconHelper(Context mContext) {
        this.mContext = mContext;
    }

    public IconHelper(Context mContext, int mSize) {
        this.mContext = mContext;
        this.mSize = mSize;
    }

    public IconHelper(Context mContext, int mSize, int mColor) {
        this.mContext = mContext;
        this.mSize = mSize;
        this.mColor = mColor;
}

    public Drawable getDrawable(IIcon iIcon){
        mDrawable = new IconicsDrawable(mContext)
                .icon(iIcon)
                .sizeDp(mSize)
                .color(mColor);
        return mDrawable;
    }

    public Drawable getDrawable(IIcon iIcon, int color){
        mDrawable = new IconicsDrawable(mContext)
                .icon(iIcon)
                .sizeDp(mSize)
                .color(color);
        return mDrawable;
    }



    public Drawable getDrawable(IIcon iIcon, int size, int color){
        mDrawable = new IconicsDrawable(mContext)
                .icon(iIcon)
                .sizeDp(size)
                .color(color);
        return mDrawable;
    }
}
