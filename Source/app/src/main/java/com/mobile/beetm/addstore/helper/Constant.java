package com.mobile.beetm.addstore.helper;

import java.util.ArrayList;
import java.util.List;

public class Constant {
    public static int NUMBER_OF_RETRIES = 3;
    public static int CONNECTION_TIME_OUT = 180;
    public static int READ_TIME_OUT = 180;
    public static int WRITE_TIME_OUT = 180;
    public static String APP_DATE_FORMAT = "yyyy-MM-dd";
    public static String APP_DAY_MONTH_FORMAT = "dd/MM";
    public static String APP_DAY_FORMAT = "EEEE";
    public static String APP_SHARED_PREFERENCE = "APP_SHARED_PREFERENCE";
    public static String APP_PREFERENCE = "APP_PREFERENCE";
    public static String API_BASE_URL = "";
    public static String API_BASE_URL_UAT = "http://www.traveylon.com/";
    public static String API_BASE_FILE_URL = "https://addstore.lk/uploads/thumbs/";
    public static String API_BASE_FILE_GALLERY_URL = "https://addstore.lk/uploads/gallery/";
    public static final String SERVER_ENDPOINT = "MasterFiles/";
    public static final String SERVER_ENDPOINT_FILE = "FileService/";
    public static String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoiZ2VuaWUuc2hpQGdtYWlsLmNvbSIsInVuaXF1ZV9uYW1lIjoiTXMuLkdFTklFIFNISSIsImlzcyI6IkdVSSBTb2x1dGlvbnMgUHRlIEx0ZCIsImF1ZCI6Imh0dHA6Ly93d3cubmVvZ2FyZGVuLmNvbS5zZy8iLCJleHAiOjE4NjA0NjQxMTgsIm5iZiI6MTU0NTEwNDExOH0.aMQygsnA0RsFBIWgrCPWjSBX75ab9orChbl0_CffhOw";

    public static String KEY_CATEGORY_ID = "category_id";
    public static String KEY_LONGITUDE = "longitude";
    public static String KEY_LATITUDE = "latitude";
    public static String KEY_CITY = "city";
    public static String KEY_STATE = "state";
    public static String KEY_TITLE = "title";
    public static String KEY_ITEM = "item";
    public static String KEY_INTERNET_CONNECTION_ISSUE = "internet";
    public static String KEY_SERVER_ISSUE = "server";
    public static String KEY_SEARCH_RESULT_NOT_FOUND = "itemNotFound";
    public static String ACTIVITY_NAME = "ACTIVITY_NAME";

    public static boolean ENABLE_IMAGE_CACHE = true;


}
