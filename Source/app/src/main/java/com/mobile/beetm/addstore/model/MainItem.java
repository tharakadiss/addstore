package com.mobile.beetm.addstore.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MainItem implements Parcelable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("rating")
    @Expose
    public float rating;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("parent_category")
    @Expose
    public String parentCategory;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phone_no")
    @Expose
    public String phoneNo;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("featured_img")
    @Expose
    public String featuredImg;
    @SerializedName("gallery")
    @Expose
    public String gallery;
    @SerializedName("opening_hour")
    @Expose
    public String openingHour;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("total_view")
    @Expose
    public String totalView;

    public Boolean isOpening;

    public List<OpeningHours> openingHours = new ArrayList<>();

    protected MainItem(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        rating = in.readFloat();
        category = in.readString();
        parentCategory = in.readString();
        address = in.readString();
        phoneNo = in.readString();
        website = in.readString();
        email = in.readString();
        state = in.readString();
        city = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        featuredImg = in.readString();
        gallery = in.readString();
        openingHour = in.readString();
        status = in.readString();
        totalView = in.readString();
        byte tmpIsOpening = in.readByte();
        isOpening = tmpIsOpening == 0 ? null : tmpIsOpening == 1;
    }

    public static final Creator<MainItem> CREATOR = new Creator<MainItem>() {
        @Override
        public MainItem createFromParcel(Parcel in) {
            return new MainItem(in);
        }

        @Override
        public MainItem[] newArray(int size) {
            return new MainItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeFloat(rating);
        dest.writeString(category);
        dest.writeString(parentCategory);
        dest.writeString(address);
        dest.writeString(phoneNo);
        dest.writeString(website);
        dest.writeString(email);
        dest.writeString(state);
        dest.writeString(city);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(featuredImg);
        dest.writeString(gallery);
        dest.writeString(openingHour);
        dest.writeString(status);
        dest.writeString(totalView);
        dest.writeByte((byte) (isOpening == null ? 0 : isOpening ? 1 : 2));
    }

    public static class OpeningHours{

        public String day;
        public String closed;
        public String start_time;
        public String close_time;
    }


}
