package com.mobile.beetm.addstore.model;

import com.google.gson.annotations.SerializedName;

public class Category {
    public String id;
    public String parent;
    public String title;
    @SerializedName("fa_icon")
    public String faIcon;
    public int drawableId;
}
